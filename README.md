# app-api

A web component that provides a wrapper for HATEOAS api.

Example:

```html
<app-api id="api"></app-api>
```

Within your pages, u can use api like:

For fetch endpoints:
```js
api.fetch({
  resource: 'reports',
  success: function(reports) {
    me.set('reports', reports);
  }
});
```

For save endpoints:
```js
api.apiRoot.postReports(report).lastRequest.completes.then(function() {
  console.log('save success');
});
```