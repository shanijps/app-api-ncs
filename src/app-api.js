Polymer({
  is: 'app-api',

  properties: {

    /**
     * The context path for RESTful endpoints, make sure to set app.appContext if this is not set explicitly.
     */
    baseUrl: {
      type: String,
      value: function() {
        return app.appContext;
      }
    },

    /**
     * The endpoint for getting user info.
     */
    statusUri: {
      type: String,
      value: '/auth/status'
    },

    /**
     * The endpoint for form based logout.
     */
    logoutUri: {
      type: String,
      value: '/auth/logout'
    }
  },

  ready: function() {
    this.async(this._ignoreErrors({
      401: [/\/auth\/status$/]
    }));
    this.async(this._checkAuthStatus);
    this.async(this._prepareApi);
  },

  _ignoreErrors: function(errorObj) {
    this.$.xhrHandler.skipPatterns = errorObj;
  },

  _checkAuthStatus: function(failure) {
    var me = this;
    var authCheck = function() {
      me.$.status.generateRequest().completes.then(function(request) {
        app.set('User', request.response);
        document.dispatchEvent(new CustomEvent('auth-success'));

        if (window.location.hash.substr(1) === '/login') {
          if (window.location.hash.substr(1) !== '/signup') {
            app.$.router.go(app.redirectUri || '/');
          }
        }
      }, failure || function() {
        if (window.location.hash.substr(1) !== '/signup') {
          app.$.router.go('/login');
        }
      });
    };

    if (document.xhrEventsPromise) {
      document.xhrEventsPromise.then(authCheck);
    } else {
      authCheck();
    }
  },

  _prepareApi: function() {
    var me = this;
    // auth promise (resolved after authentication is successful)
    app.authPromise = new Promise(function(authResolve) {
      me.authResolve = authResolve;
    });
    // api promise (resolved after successful response of rootResource)
    var apiPromise = new Promise(function(apiResolve) {
      app.authPromise.then(function() {
        me.$.rootResource.generateRequest().completes.then(function(request) {
          apiResolve(request.response);
        });
      });
    });

    var optionsArray = [];
    me.fetch = function(options) {
      optionsArray.push(options);
      apiPromise.then(function(root) {
        _.forEach(optionsArray, function(options) {
          root = options.root || root;
          var handler = root[options.resource + 'Handler'];
          if (!handler) {
            console.warn('Could not find handler:', options.resource);
          } else {
            if (options.id !== undefined && options.id !== null) {
              handler = handler._createSubInstance();
              handler.url = root[options.resource + 'Handler'].url + '/' + options.id;
            }
            if (options.params !== undefined || options.params !== null) {
              handler.params = options.params;
            }
            if (options.headers) {
              handler.headers = options.headers;
            }
            var success = (options.success || _.noop).bind(options.scope);
            var failure = (options.failure || _.noop).bind(options.scope);

            handler.generateRequest().completes.then(function(request) {
              if (request.response[handler.embeddedProperty]) {
                success(request.response[options.resource] || request.response[options.entityName], request.response);
              } else {
                success(request.response);
              }
            }, failure);
          }
        });
        optionsArray = [];
      });

      setTimeout(function() {
        if (!me.User && !me.$.rootResource.lastResponse) {
          me._checkAuthStatus(function() {
            optionsArray = [];
          });
        }
      });
    };

    document.addEventListener('auth-success', me.authResolve);
  },

  attached: function() {
    var me = this;

    document.addEventListener('xhr-error', function(event) {
      if (event.detail && event.detail.request && event.detail.request.status) {
        if (401 === event.detail.request.status && event.detail.request.url.indexOf(app.appContext) !== -1) {
          me._postLogout(event.detail.request, 'sessionTimedOut');
        }
      }
    });
  },

  /**
   * Triggers logout
   */
  logout: function() {
    this.$.logout.generateRequest().completes.then(this._postLogout);
  },

  _postLogout: function(request, logoutContext) {
    delete app.User;
    if (logoutContext === 'sessionTimedOut' && window.location.hash.substr(1) !== '/signup') {
      app.redirectUri = window.location.hash.substr(1);
    } else {
      delete app.redirectUri;
    }
    if ('/login' === app.redirectUri) {
      delete app.redirectUri;
    }
    if (request && request.xhr) {
      app.oidcAuth = Boolean(request.xhr.getResponseHeader('oidc-auth'));
    }
    if (window.location.hash.substr(1) !== '/signup') {
      app.$.router.go('/login');
    }
  }
});
